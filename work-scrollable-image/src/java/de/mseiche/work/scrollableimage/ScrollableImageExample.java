package de.mseiche.work.scrollableimage;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ScrollableImageExample extends JFrame {

	public ScrollableImageExample() throws IOException {
		super("Scrollable-Image-Example");
		initialize();
	}

	private void initialize() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);

		final JPanel contentPanel = new JPanel(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1D;
		constraints.weighty = 1D;
		final ScrollableImagePanel imagePanel = new ScrollableImagePanel();
		imagePanel.setImage(ImageIO.read(getClass().getResourceAsStream("/example-image.jpg")));
		imagePanel.setZoomFactor(1.5D);
		contentPanel.add(imagePanel, constraints);

		setContentPane(contentPanel);
		setLocationRelativeTo(null);
	}

	public static final void main(final String[] args) throws IOException {
		new ScrollableImageExample().setVisible(true);
	}
}
