package de.mseiche.work.scrollableimage;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public final class ScrollableImageMouseWheelListener implements MouseWheelListener {

	private final ScrollableImagePanel scrollableImagePanel;

	public ScrollableImageMouseWheelListener(final ScrollableImagePanel scrollableImagePanel) {
		super();
		this.scrollableImagePanel = scrollableImagePanel;
	}

	@Override
	public void mouseWheelMoved(final MouseWheelEvent mouseWheelEvent) {
		scrollableImagePanel.setZoomFactor(scrollableImagePanel.getZoomFactor()
				+ mouseWheelEvent.getWheelRotation() * 0.05D * scrollableImagePanel.getZoomFactor());
	}
}
