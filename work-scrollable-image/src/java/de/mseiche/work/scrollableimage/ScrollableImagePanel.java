package de.mseiche.work.scrollableimage;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * Dient der Anzeige eines Bildes mit Zoom-Funktionalit&auml;t in Swing
 * 
 * @author Manuel Seiche
 * @since 20.01.2019
 */
@SuppressWarnings("serial")
public final class ScrollableImagePanel extends JPanel {

	/**
	 * Diese {@link Color Farbe} wird hinter dem zu zeichnenden {@link #image}
	 * angezeigt. In vielen F&auml;llen wird man die Farbe nie sehen.
	 */
	private final Color backgroundColor;

	/**
	 * Das darzustellende {@link BufferedImage}
	 */
	private BufferedImage image;

	/**
	 * Der Zoom-Faktor kann <code>null</code> sein. In diesem Fall sorgt das
	 * daf&uuml;r, dass beim Rendering Defaults auf Basis der aktuellen
	 * Gr&ouml;&szlig;e von {@link #image} und {@link ScrollableImagePanel} bestimmt
	 * wird.
	 */
	private Double zoomFactor;

	/**
	 * Erzeugt ein {@link ScrollableImagePanel} mit {@link Color#WHITE wei&szlig;em}
	 * {@link #backgroundColor Hintergrund}.
	 */
	public ScrollableImagePanel() {
		this(Color.WHITE);
	}

	/**
	 * @param backgroundColor
	 *            {@link #backgroundColor}
	 */
	public ScrollableImagePanel(final Color backgroundColor) {
		super();
		this.backgroundColor = backgroundColor;
		initialize();
	}

	/**
	 * Registriert alle Listener. M&ouml;glich, dass hier in Zukuft noch was
	 * f&uuml;r die Translation kommen muss...
	 */
	private void initialize() {
		final ScrollableImageMouseWheelListener mouseAdapter = new ScrollableImageMouseWheelListener(this);
		addMouseWheelListener(mouseAdapter);
	}

	@Override
	protected void paintComponent(final Graphics graphics) {
		super.paintComponent(graphics);
		final Graphics2D graphics2d = (Graphics2D) graphics;
		// Initialisiere die Hintergrundfarbe
		graphics2d.setColor(backgroundColor);
		graphics2d.fillRect(0, 0, getWidth(), getHeight());

		/*
		 * Haben wir kein Bild, koennen wir keines darstellen. In diesem Fall brechen
		 * wir nun ab.
		 */
		if (image == null) {
			return;
		}

		/*
		 * Bicubic Interpolation => Bessere Farbkanten bei Veraenderung der
		 * Bildskalierung als mit Bilinear Interpolation.
		 */
		graphics2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

		final double currentWidth = getWidth();
		final double currentHeight = getHeight();

		final double imageWidth = image.getWidth();
		final double imageHeight = image.getHeight();

		final double minScaleX = currentWidth / imageWidth;
		final double minScaleY = currentHeight / imageHeight;

		if (zoomFactor == null) {
			zoomFactor = Math.max(minScaleX, minScaleY);
		} else {
			zoomFactor = Math.max(zoomFactor, Math.max(minScaleX, minScaleY));
		}

		/*
		 * Affine Transformation: SRT (Scale, Rotate, Translate) zusammengefasst in
		 * einer Matrix. Eine Instanz von AffineTransform ist demnach nichts weiter, als
		 * eine zwei-/dreidimensionale Double-Matrix.
		 * 
		 * Kurzer Fun-Fact: Die hier hinzugefuegten Transformationen werden bei der
		 * Berechung in umgekehrter Reihenfolge verarbeitet :-)
		 */
		final AffineTransform transform = new AffineTransform();
		transform.setToIdentity();
		transform.translate(currentWidth / 2, currentHeight / 2);
		transform.scale(zoomFactor, zoomFactor);
		transform.translate(-imageWidth / 2, -imageHeight / 2);
		graphics2d.drawRenderedImage(image, transform);
	}

	/**
	 * @param image
	 *            neuer Wert f&uuml;r {@link #image}
	 */
	public void setImage(final BufferedImage image) {
		this.image = image;
		// Bisherige Transformationen rueckgaengig machen
		zoomFactor = null;
		repaint();
	}

	/**
	 * @return {@link #zoomFactor}
	 */
	public double getZoomFactor() {
		return zoomFactor;
	}

	/**
	 * @param zoomFactor
	 *            Neuer Wert f&uuml;r {@link #zoomFactor}
	 */
	public void setZoomFactor(final double zoomFactor) {
		this.zoomFactor = zoomFactor;
		repaint();
	}
}
